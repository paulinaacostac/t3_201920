package view;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{
	}

	public void printMenu()
	{
		System.out.println("1. Cargar archivo CSV");
		System.out.println("2. Consultar viajes por hora");
		System.out.println("3. Ordenar Ascendentemente por ShellSort");
		System.out.println("4. Ordenar Ascendentemente por MergeSort");
		System.out.println("5. Ordenar Ascendentemente por QuickSort");
		System.out.println("6. Exit");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void printCargarViajes(int pLineas, Viaje pPrimerViajeCargado, Viaje pUltimoViajeCargado)
	{
		System.out.println(
				"1. Numero de viajes cargados: "+pLineas+"\n"+
						"2. Primer Viaje Cargado: "+"Dstid: "+pPrimerViajeCargado.darDstid()+" Sourceid: "+pPrimerViajeCargado.darSourceid()+" Mean_Travel_Time: "+pPrimerViajeCargado.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pPrimerViajeCargado.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pPrimerViajeCargado.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pPrimerViajeCargado.darGeometricStandardDeviationTravelTime()+"\n"+
						"3. Ultimo Viaje Cargado: "+"Dstid: "+pPrimerViajeCargado.darDstid()+" Sourceid: "+pUltimoViajeCargado.darSourceid()+" Mean_Travel_Time: "+pUltimoViajeCargado.darMeanTravelTime()+" Standard_Deviation_Travel_Time: "+pUltimoViajeCargado.darStandardDeviationTravelTime()+" Geometric_Mean_Travel_Time: "+pUltimoViajeCargado.darGeometricMeanTravelTime()+" Geometric_Standard_Deviation_Travel_Time: "+pUltimoViajeCargado.darGeometricStandardDeviationTravelTime()+"\n");
	}


	public void printMessage(String mensaje) {

		System.out.println(mensaje);
	}		


	public void printResultadoOrdenamiento(long pDuracion, ArregloDinamico<Viaje> pViajes, String pAlgoritmo) 
	{
		System.out.println("Tiempo de ordenamiento "+ pAlgoritmo + ": " + pDuracion + " milisegundos");
		int i = 0;
		System.out.println("Los diez primeros viajes son: ");
		System.out.println("SourceID | DestID | Hour | Mean Travel Time | Standard Deviation Travel Time");
		while(i<10 && i<pViajes.darTamano())
		{
			Viaje viajeTemp = pViajes.darElemento(i);
			String viajeString = viajeTemp.darSourceid() + " | " + viajeTemp.darDstid() + " | " + viajeTemp.darHod() + " | " + viajeTemp.darMeanTravelTime() + " | " + viajeTemp.darStandardDeviationTravelTime();
			System.out.println(viajeString);
			i++;
		}
		i = pViajes.darTamano()-10;
		System.out.println("Los diez �ltimos viajes son: ");
		System.out.println("SourceID | DestID | Hour | Mean Travel Time | Standard Deviation Travel Time");
		while(i>0 && i<pViajes.darTamano())
		{
			Viaje viajeTemp = pViajes.darElemento(i);
			String viajeString = viajeTemp.darSourceid() + " | " + viajeTemp.darDstid() + " | " + viajeTemp.darHod() + " | " + viajeTemp.darMeanTravelTime() + " | " + viajeTemp.darStandardDeviationTravelTime(); 
			System.out.println(viajeString);
			i++;
		}
	}

	public void printConsultaPorHora(int pNumeroViajes, int pHora) 
	{
		String mensaje = "El n�mero de viajes para la hora " + pHora +" es: " + pNumeroViajes;
		System.out.println(mensaje);
	}		


}
