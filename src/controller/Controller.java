package controller;

import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		ArregloDinamico<Viaje> viajesHora=null;
		String dato2 = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				modelo = new MVCModelo();
				modelo.cargar();
				System.out.println("Archivo CSV cargado");
				view.printCargarViajes(modelo.darLineas(), modelo.darPrimeroCargado(), modelo.darUltimoCargado());
				break;

			case 2:
				System.out.println("--------- \nDar hora: ");
				int hora = lector.nextInt();
				viajesHora =modelo.consultarViajesPorHora(hora);
				view.printConsultaPorHora(viajesHora.darTamano(), hora);
				break;

			case 3:
				long startTime1 = System.currentTimeMillis(); // medici�n tiempo actual
				modelo.ordenarShellSort(modelo.darViajes());
				long endTime1 = System.currentTimeMillis(); // medici�n tiempo actual
				long duration1 = endTime1 - startTime1; // duracion de ejecucion del algoritmo
				view.printResultadoOrdenamiento(duration1, modelo.darViajes(), "ShellSort");
				break;

			case 4:
				long startTime2 = System.currentTimeMillis(); // medici�n tiempo actual
				modelo.ordenarMergeSort(modelo.darViajes());
				long endTime2 = System.currentTimeMillis(); // medici�n tiempo actual
				long duration2 = endTime2 - startTime2; // duracion de ejecucion del algoritmo
				view.printResultadoOrdenamiento(duration2, modelo.darViajes(), "MergeSort");
				break;	

			case 5:
				long startTime = System.currentTimeMillis(); // medici�n tiempo actual
				modelo.ordenarQuickSort(modelo.darViajes());
				long endTime = System.currentTimeMillis(); // medici�n tiempo actual
				long duration = endTime - startTime; // duracion de ejecucion del algoritmo
				view.printResultadoOrdenamiento(duration, modelo.darViajes(), "QuickSort");
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
