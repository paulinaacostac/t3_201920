
package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private int lineas;
	private Viaje primerViaje;
	private Viaje ultimoViaje;
	private ArregloDinamico<Viaje> viajes;

	public MVCModelo()
	{
		lineas=0;
		primerViaje=null;
		ultimoViaje=null;
		viajes=new ArregloDinamico<Viaje>(20000000);
	}
	public void cargar()
	{
		CSVReader reader = null;
		int num=0;
		Viaje viaje=null;
		try 
		{
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
			reader.skip(1);
			for(String[] nextLine : reader) 
			{
				viaje = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				viajes.agregar(viaje);
				if (viajes.darTamano()==1)
				{
					primerViaje=viaje;
				}
			}
			lineas=viajes.darTamano();
			ultimoViaje=viaje;

		}

		catch (FileNotFoundException e)  
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int darLineas()
	{
		return lineas;
	}

	public ArregloDinamico<Viaje> consultarViajesPorHora(int pHora)
	{
		ArregloDinamico<Viaje> viajesHora = new ArregloDinamico<Viaje>(100);
		for (int i = 0; i < viajes.darTamano(); i++) 
		{
			Viaje viaje = viajes.darElemento(i);
			if (viaje.darHod() == pHora) 
			{
				viajesHora.agregar(viaje);
			}
		}
		return viajesHora;
	}

	public Viaje darUltimoCargado()
	{
		return ultimoViaje;
	}

	public Viaje darPrimeroCargado()
	{
		return primerViaje;
	}
	
	public void ordenarShellSort(ArregloDinamico<Viaje> a)
	{
		int N = a.darTamano();

		int h = 1;
		while (h< N/3) 
		{
			h = 3*h +1;
		}
		while (h>=1) 
		{
			for (int i = h; i < N; i++) 
			{
				for (int j = i; j >= h && a.darElemento(j).compareTo(a.darElemento(j-h))<0; j-=h) 
				{
					a.exch(j, j-h);
				}
			}
			h = h/3;
		}
	}
	public void ordenarMergeSort(ArregloDinamico<Viaje> a)
	{
		ArregloDinamico<Viaje> aux = new ArregloDinamico<Viaje>(a.darTamano());
		sortMerge(a, aux, 0, a.darTamano()-1);
	}
	
	public void sortMerge(ArregloDinamico<Viaje> a,ArregloDinamico<Viaje> aux, int lo, int hi )
	{
		if(hi<=lo) return;
		int mid = lo + (hi - lo)/2;
		sortMerge(a, aux, lo, mid);
		sortMerge(a, aux, mid+1, hi);
		merge(a, aux, lo, mid, hi);
		
	}
	public void merge(ArregloDinamico<Viaje> a,ArregloDinamico<Viaje> aux, int lo,int mid, int hi )
	{
		for (int k = lo; k <=hi; k++) 
		{
			aux.set(a.darElemento(k), k);
		}
		int i = lo;
		int j = mid+1;
		for (int j2 = lo; j2 <= hi; j2++) 
		{
			if 		(i>mid) a.set(aux.darElemento(j++), j2);
			else if (j>hi ) a.set(aux.darElemento(i++), j2);
			else if (aux.darElemento(j).compareTo(aux.darElemento(i))<0 ) a.set(aux.darElemento(j++), j2);
			else			a.set(aux.darElemento(i++), j2);
		}
	}
	private int particion(ArregloDinamico<Viaje> a,int lo, int hi)
	{
		int i = lo, j = hi+1; 
		Viaje v = a.darElemento(lo);
		while (true)
		{ 
			while (a.darElemento(++i).compareTo(v)<0) if (i == hi) break;
			while (v.compareTo(a.darElemento(--j))<0) if (j == lo) break;
			if (i >= j) break;
			a.exch(i, j);
		}
		a.exch(lo, j); 
		return j; 

	}
	public void ordenarQuickSort(ArregloDinamico<Viaje> a)
	{
//		a.shuffle();
		sort(a,0, a.darTamano()-1);
	}

	public void sort(ArregloDinamico<Viaje> a,int lo, int hi)
	{
		if (hi <= lo) return;
		int j = particion(a,lo,hi);
		sort(a,lo, j-1);
		sort(a,j+1, hi);
	}
	public ArregloDinamico<Viaje> darViajes()
	{
		return viajes;
	}
	public void cambiarViajes(ArregloDinamico<Viaje>a)
	{
		viajes=a;
	}
}
