package model.data_structures;

public class LinkedList<T extends Comparable<T>>
{
	private Node<T> primerNodo;
	private int tamano;
	private Node<T>ultimoNodo;
	private Node<T>actual;


	public LinkedList()
	{
		primerNodo=null;
		ultimoNodo=null;
		actual=null;
		tamano=0;
	}
	public void agregar(T dato)
	{
		Node<T> elementoActual = primerNodo;
		Node<T> nuevo = new Node<T>(dato);
		if (primerNodo == null)
		{
			primerNodo = nuevo;
			ultimoNodo = nuevo;
			tamano++;
		}
		else 
		{
			ultimoNodo.cambiarSiguiente(nuevo);
			ultimoNodo=nuevo;
			tamano++;
		}
		
	}
	public T buscar(T dato) 
	{
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		Node<T> elementoActual = primerNodo;
		while (elementoActual!= null && dato.compareTo(elementoActual.darElemento())!=0)
		{
			elementoActual = elementoActual.darSiguiente();
		}
		return elementoActual.darElemento();
	}
	public T eliminar(T dato) 
	{
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.
		Node<T> nodoActual = primerNodo;
		if (dato.compareTo(nodoActual.darElemento())==0)
		{
			primerNodo = primerNodo.darSiguiente();
			tamano--;
		}
		else
		{
			nodoActual =primerNodo.darSiguiente();
			while(nodoActual != null)
			{
				if (dato.compareTo(nodoActual.darSiguiente().darElemento())==0)
				{
					nodoActual.cambiarSiguiente(nodoActual.darSiguiente());
					tamano--;
				}
				actual=nodoActual;
				nodoActual = nodoActual.darSiguiente();

			}
		}
		return nodoActual.darElemento();
	}
	public T darElemento(int i) 
	{
		Node<T> nodoActual = primerNodo;
		// TODO implementar
		for (int j=0;j<i;j++)
		{
			nodoActual=nodoActual.darSiguiente();
		}
		return nodoActual.darElemento();
	}
	public int darTamano()
	{
		return tamano;
	}
	public void avanzar()
	{
		actual.darSiguiente();
		
	}
	public T darNodoActual()
	{
		return actual.darElemento();
	}
	public Node<T> darPrimerNodo()
	{
		return primerNodo;
	}
	public void retroceder()
	{
		Node<T> act=primerNodo;
		while(act.darSiguiente()!=null)
		{
			if (act.darSiguiente().darElemento().compareTo(actual.darElemento())==0)
			{
				actual=act;
			}
		}
	}


}
