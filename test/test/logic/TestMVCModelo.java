package test.logic;

import static org.junit.Assert.*;
import java.lang.Math; 
import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;

import org.junit.Before;
import org.junit.Test;

public class TestMVCModelo 
{
	
	private MVCModelo modelo;
	private ArregloDinamico<Viaje> arreglo;
	private static int CAPACIDAD=100;
	
	@Before
	public void setUp1() 
	{
		modelo= new MVCModelo();
		arreglo = new ArregloDinamico<Viaje>(CAPACIDAD);
	}

	public void setUp2() 
	{	
		modelo= new MVCModelo();
		arreglo=new ArregloDinamico<Viaje>(CAPACIDAD);
		Viaje v= null;
		//Datos ordenados ascendentemente
		for(int i =0; i< CAPACIDAD;i++)
		{
			v=new Viaje(0,0,0,i,0,0,0);
			arreglo.agregar(v);
		}
	}
	public void setUp3() 
	{
		modelo= new MVCModelo();
		arreglo=new ArregloDinamico<Viaje>(CAPACIDAD);
		Viaje v= null;
		//Datos ordenados descendentemente
		for(int i =CAPACIDAD-1; i>=0;i--)
		{
			v=new Viaje(0,0,0,i,0,0,0);
			arreglo.agregar(v);
			
		}
	}
	public void setUp4() 
	{
		modelo= new MVCModelo();
		arreglo=new ArregloDinamico<Viaje>(CAPACIDAD);
		Viaje v= null;
		//Datos desordenados
		for(int i =0; i< CAPACIDAD;i++)
		{
			v=new Viaje(0,0,0,i,0,0,0);
			arreglo.agregar(v);
		}
		arreglo.shuffle();
	}

	@Test
	public void testOrdenarQuickSortAscendentemente() 
	{
		setUp2();
		modelo.ordenarQuickSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
	}
	@Test
	public void testOrdenarQuickSortDescendentemente() 
	{
		setUp3();
		modelo.ordenarQuickSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
	}
	@Test
	public void testOrdenarQuickSortDesordenados() 
	{
		setUp4();
		modelo.ordenarQuickSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
		
	}
	@Test
	public void testOrdenarMergeSortAscendentemente() 
	{
		setUp2();
		modelo.ordenarMergeSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
	}
	@Test
	public void testOrdenarMergeSortDescendentemente() 
	{
		setUp3();
		modelo.ordenarMergeSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);	
	}
	@Test
	public void testOrdenarMergeSortDesordenados() 
	{
		setUp4();
		modelo.ordenarMergeSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
		
	}
	
	@Test
	public void testOrdenarShellSortAscendentemente() 
	{
		setUp2();
		modelo.ordenarShellSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);
	}
	@Test
	public void testOrdenarShellSortDescendentemente() 
	{
		setUp3();
		modelo.ordenarShellSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);	
	}
	@Test
	public void testOrdenarShellSortDesordenados() 
	{
		setUp4();
		modelo.ordenarShellSort(arreglo);
		double primero=arreglo.darElemento(0).darMeanTravelTime();
		double ultimo=arreglo.darElemento(99).darMeanTravelTime();
		double mitad=arreglo.darElemento(50).darMeanTravelTime();
		assertTrue(0.0==primero);
		assertTrue(99.0==ultimo);
		assertTrue(50==mitad);	
	}

}
